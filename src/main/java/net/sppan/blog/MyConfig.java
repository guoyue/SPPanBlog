package net.sppan.blog;

import java.util.Properties;

import net.sppan.blog.controller.AdminRoutes;
import net.sppan.blog.controller.ApiRouters;
import net.sppan.blog.controller.WebRouters;
import net.sppan.blog.handler.SomethingHandler;
import net.sppan.blog.interceptor.AuthorityInterceptor;
import net.sppan.blog.model.Res;
import net.sppan.blog.model._MappingKit;

import com.alibaba.druid.filter.logging.Log4jFilter;
import com.baidu.ueditor.UeditorConfigKit;
import com.baidu.ueditor.manager.DefaultFileManager;
import com.baidu.ueditor.manager.QiniuFileManager;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.ModelRecordElResolver;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.FreeMarkerRender;
import com.jfinal.render.ViewType;

import freemarker.template.TemplateModelException;

public class MyConfig extends JFinalConfig{


	private Boolean isDev = false;

	/**
	 * 常量配置
	 */
	@Override
	public void configConstant(Constants me) {
		loadPropertyFile("config.txt");
		
		isDev  = getPropertyToBoolean("isdev", false);
		// 开发模式
		me.setDevMode(isDev);
		
		me.setViewType(ViewType.FREE_MARKER);
		
		me.setBaseViewPath("/WEB-INF/views");
		
		//freemarker 模板后缀名
		me.setFreeMarkerViewExtension(".html");
		
		// 401,403,404,500错误代码处理
	}

	/**
	 * 路由配置
	 */
	@Override
	public void configRoute(Routes me) {
		me.add(new AdminRoutes());
		me.add(new ApiRouters());
		me.add(new WebRouters());
	}

	/**
	 * 全局拦截器
	 */
	@Override
	public void configInterceptor(Interceptors me) {
		me.add(new AuthorityInterceptor());
	}

	/**
	 * 配置处理器
	 */
	@Override
	public void configHandler(Handlers me) {
		me.add(new ContextPathHandler("ctx"));
		me.add(new SomethingHandler());
	}
	
	/**
	 * 配置插件
	 */
	@Override
	public void configPlugin(Plugins me) {
		// 数据库信息
		String jdbcUrl  = getProperty("db.jdbcUrl");
		String user	 = getProperty("db.user");
		String password = getProperty("db.password");
		
		// 配置Druid数据库连接池插件
		DruidPlugin druidPlugin = new DruidPlugin(jdbcUrl, user, password);
		druidPlugin.setFilters("stat,wall");
		druidPlugin.addFilter(new Log4jFilter());
		me.add(druidPlugin);

		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin).setShowSql(isDev);
		
		_MappingKit.mapping(arp);
		me.add(arp);
		// EhCache
		me.add(new EhCachePlugin());
	}

	@Override
	public void afterJFinalStart() {
		ModelRecordElResolver.setResolveBeanAsModel(true);

		//用qiniu云储存
		String ak = getProperty("qiniu.ak");
		String sk = getProperty("qiniu.sk");
		String bucket = getProperty("qiniu.bucket");
		String url = getProperty("qiniu.url");
		Boolean qiniuUse = getPropertyToBoolean("qiniu.use");
		if(qiniuUse){
			UeditorConfigKit.setFileManager(new QiniuFileManager(ak, sk, bucket));
		}else{
			url = JFinal.me().getContextPath();
			UeditorConfigKit.setFileManager(new DefaultFileManager());
		}
		
		//由于我们用到freemarker，所以在此进行freemarker配置文件的装载
		Properties p = loadPropertyFile("freemarker.properties");
		FreeMarkerRender.setProperties(p);
//		FreeMarkerRender.setTemplateLoadingPath("/WEB-INF/views");
		try {
			FreeMarkerRender.getConfiguration().setSharedVariable("menu",Res.dao.getMenuAll());
			FreeMarkerRender.getConfiguration().setSharedVariable("fileUrl",url);
		} catch (TemplateModelException e) {
			e.printStackTrace();
		}
		
	}
}
