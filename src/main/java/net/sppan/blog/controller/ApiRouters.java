package net.sppan.blog.controller;

import net.dreamlu.controller.UeditorApiController;

import com.jfinal.config.Routes;

public class ApiRouters extends Routes {

	@Override
	public void config() {
		// ueditor
		add("/ueditor/api", UeditorApiController.class);
	}

}
