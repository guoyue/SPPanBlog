package net.sppan.blog.controller;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;

public class BaseController extends Controller{

	/**
	 * 返回ajax请求结果以及相应信息
	 * @param isSuccess
	 * @param msg
	 */
	public void renderJson(boolean isSuccess, String msg) {
		Record record  = new Record();
		record.set("isSuccess", isSuccess);
		record.set("msg", msg);
		super.renderJson(record);
	}
	
}
