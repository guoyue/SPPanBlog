package net.sppan.blog.controller.admin;

import java.util.HashMap;
import java.util.List;

import net.sppan.blog.annotation.RequiresPermissions;
import net.sppan.blog.controller.BaseController;
import net.sppan.blog.model.Blog;
import net.sppan.blog.model.BlogTag;
import net.sppan.blog.model.Tags;
import net.sppan.blog.model.User;
import net.sppan.blog.utils.HtmlFilter;
import net.sppan.blog.utils.WebUtils;

import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheName;
import com.jfinal.plugin.ehcache.EvictInterceptor;

@RequiresPermissions(key={"admin:content:blog"})
public class AdminBlogController extends BaseController {
	
	@RequiresPermissions(key={"admin:content:blog:list"})
	public void list(){
		int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 10);
        String search = getPara("search");
        HashMap<String, Object> parameter = new HashMap<String, Object>();
        if(StrKit.notBlank(search)){
        	parameter.put("keyword", search);
        	setAttr("search", search);
        }
		Page<Blog> page = Blog.dao.pageNoContent(pageNumber, pageSize, parameter);
		setAttr("pageList", page);
		render("list.html");
	}
	
	@RequiresPermissions(key={"admin:content:blog:save"})
	public void add_edit(){
		Integer id = getParaToInt("id");
		if(id != null){
			Blog blog = Blog.dao.findById(id);
			setAttr("blog", blog);
			
			List<String> list =  Tags.dao.findNamesByBlogId(blog.getId());
			String tags = "";
			for (String tag : list) {
				tags += tag + ",";
			}
			if(tags.length() > 0){
				tags.substring(0, tags.length() -1);
			}
			setAttr("tags", tags);
		}
		render("form.html");
	}
	
	@RequiresPermissions(key={"admin:content:blog:save"})
	@Before(EvictInterceptor.class)
	@CacheName("blog")
	public void save_update() {
		Blog blog = getModel(Blog.class);
		// seo 添加图片alt信息
		String title = blog.getTitle();
		String content = blog.getContent();
		
		if (StrKit.notBlank(title, content)) {
			blog.setContent(content.replaceAll("alt=\"\"", "alt=\"" + title + "\""));
			blog.setDesc(HtmlFilter.truncate(blog.getContent(),300));
		}
		boolean state = false;
		User user = WebUtils.currentUser(this);
		if(null == blog.getId()){
			blog.setUserId(user.getId());
			state = blog.save();
		} else {
			state = blog.update();
		}
		Integer blogId = blog.getId();
		
		Tags.dao.deleteByBlogId(blogId);
		
		String tags = getPara("tags");
		String[] tagsArr = tags.split(",");
		Tags findTag;
		BlogTag blogTag;
		for (String tagName : tagsArr) {
			findTag = Tags.dao.findByTagName(tagName);
			//如果标签不存在，则增加标签
			if(findTag == null){
				//先保存标签
				findTag = new Tags();
				findTag.setTagName(tagName);
				findTag.save();
			}
			
			//保存对应关系
			blogTag = new BlogTag();
			blogTag.setBlogId(blogId);
			blogTag.setTagId(findTag.getId());
			blogTag.save();
		}
		if(state){
			renderJson(true, "保存成功");
		}else{
			renderJson(true, "保存失败");
		}
	}
	
	@RequiresPermissions(key={"admin:content:blog:delete"})
	@Before(EvictInterceptor.class)
	@CacheName("blog")
	public void delete(){
		Integer id = getParaToInt("id");
		Tags.dao.deleteByBlogId(id);
		boolean isSuccess = Blog.dao.deleteById(id);
		if(isSuccess){
			renderJson(isSuccess, "删除成功");
		}else{
			renderJson(isSuccess, "删除失败");
		}
	}
	
	/**
	 * 切换显示状态
	 */
	@RequiresPermissions(key={"admin:content:blog:save"})
	@Before(EvictInterceptor.class)
	@CacheName("blog")
	public void switch_view(){
		Integer id = getParaToInt("id");
		Blog blog = Blog.dao.findById(id);
		boolean isSuccess = false;
		if(StrKit.notNull(blog)){
			int status = blog.getDelStatus();
			blog.setDelStatus(status == 1 ? 0 : 1);
			isSuccess = blog.update();
		}
		if(isSuccess){
			renderJson(isSuccess, "切换成功");
		}else{
			renderJson(isSuccess, "切换失败");
		}
	}
}
