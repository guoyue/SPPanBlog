package net.sppan.blog.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sppan.blog.commons.Constants;
import net.sppan.blog.model.base.BaseBlog;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jfinal.plugin.ehcache.IDataLoader;

/**
 * 博客实体
 * 
 * @author SPPan
 */
@SuppressWarnings("serial")
public class Blog extends BaseBlog<Blog> {
	public static final Blog dao = new Blog();

	/**
	 * 分页查找不带文章内容
	 * @param @param pageNum
	 * @param @param pageSize
	 * @param @return	设定文件
	 * @return Page<Blog>	返回类型
	 * @throws
	 */
	public Page<Blog> pageNoContent(Integer pageNum, int pageSize, Map<String, Object> parameter) {
		final List<Object> parameters = new ArrayList<Object>();
		String select = "SELECT b.id, b.title, b.view_count, b.blog_type, b.del_status, b.desc,b.create_time, b.update_time, u.name ";
		StringBuilder sqlOutSelect = new StringBuilder("FROM sys_blog AS b ,sys_user AS u WHERE b.user_id = u.id ");
		if(parameter.containsKey("keyword")){
			sqlOutSelect.append(" AND (b.title like ? OR b.content like ?) ");
			parameters.add("%" + parameter.get("keyword") + "%");
			parameters.add("%" + parameter.get("keyword") + "%");
		}
		if(parameter.containsKey("type")){
			sqlOutSelect.append(" AND b.blog_type = ?");
			parameters.add(parameter.get("type"));
		}
		if(parameter.containsKey("status")){
			sqlOutSelect.append(" AND b.del_status = ?");
			parameters.add(parameter.get("status"));
		}
		sqlOutSelect.append(" ORDER BY b.id DESC");
		return this.paginate(pageNum, pageSize, select, sqlOutSelect.toString(), parameters.toArray());
	}
	
	/**
	 * 根据标签分页查找不带文章内容
	 * @param @param pageNum
	 * @param @param pageSize
	 * @param @return	设定文件
	 * @return Page<Blog>	返回类型
	 * @throws
	 */
	public Page<Blog> pageNoContentByTag(Integer pageNum, int pageSize,String tagName) {
		final List<Object> parameters = new ArrayList<Object>();
		String select = "SELECT b.id, b.title, b.view_count, b.blog_type, b.del_status, b.desc,b.create_time, b.update_time,b.user_id, u.name";
		StringBuilder sqlOutSelect = new StringBuilder(" FROM sys_blog as  b LEFT JOIN sys_user u ON b.user_id = u.id ");
		sqlOutSelect.append(" WHERE b.del_status = 0 AND b.id IN (SELECT bt.blog_id FROM sys_blog_tag as bt,sys_tags as st WHERE bt.tag_id = st.id AND st.tag_name = ?)");
		parameters.add(tagName);
		sqlOutSelect.append(" ORDER BY b.id DESC");
		return this.paginate(pageNum, pageSize, select, sqlOutSelect.toString(), parameters.toArray());
	}

	/**
	 * 根据id查找blog相关信息
	 * @param @param paraToInt
	 * @param @return	设定文件
	 * @return Blog	返回类型
	 * @throws
	 */
	public Blog findFullById(Integer blogId) {
		updateViewCountById(blogId);
		String sql = "SELECT b.*, u.name FROM sys_blog AS b ,sys_user AS u WHERE b.id = ? AND b.user_id = u.id AND b.del_status = 0 limit 1";
		Blog blog = this.findFirst(sql, blogId);
		return blog;
	}


	/**
	 * 保存blog实体
	 */
	@Override
	public boolean save() {
		this.setCreateTime(new Date());
		this.setUpdateTime(new Date());
		return super.save();
	}

	/**
	 * 更新blog实体
	 */
	@Override
	public boolean update() {
		this.setUpdateTime(new Date());
		return super.update();
	}

	/**
	 * 更新博客的浏览次数
	 * @param id
	 * @return
	 */
	public static boolean updateViewCountById(Integer id){
		String sql = "UPDATE sys_blog b SET b.view_count = b.view_count + 1 WHERE b.id = ?";
		int count = Db.update(sql, id);
		return count > 0;
	}
	
	/**
	 * 获取最热门的5条博客ID和title
	 * @return
	 */
	public List<Blog> findTop5(){
		return CacheKit.get(Constants.CacheName.top.get(), "_top5_blog", new IDataLoader() {
			@Override
			public List<Blog> load() {
				String sql = "SELECT id, title FROM sys_blog ORDER BY view_count DESC LIMIT 5";
				return find(sql);
			}
		});
	}
}
