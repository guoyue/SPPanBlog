package net.sppan.blog.handler;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sppan.blog.model.Blog;
import net.sppan.blog.model.Tags;
import net.sppan.blog.model.User;
import net.sppan.blog.utils.WebUtils;

import com.jfinal.handler.Handler;

public class SomethingHandler extends Handler {

	@Override
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		if(target.startsWith("/admin")){
			//管理后台用户信息
			User user = WebUtils.currentUser(request, response);
			if(user != null && user.getId() != null){
				request.setAttribute("currentUser", user);
			}
		}else{
			//热门博客推荐
			List<Blog> top5 = Blog.dao.findTop5();
			request.setAttribute("top5", top5);
			
			//侧栏标签
			List<String> topTags = Tags.dao.findAllNameList();
			request.setAttribute("topTags", topTags);
		}
		next.handle(target, request, response, isHandled);
	}

}
